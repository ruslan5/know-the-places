<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%reservations}}`.
 */
class m220402_112622_create_reservations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reservations', [
            'id' => $this->primaryKey(),
            'date_from' => $this->date()->notNull(),
            'date_to' => $this->date()->notNull(),
            'room_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('reserved_rooms', 'reservations', 'room_id', 'rooms', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('reserved_rooms', 'reservations');
        $this->dropTable('reservations');
    }
}
