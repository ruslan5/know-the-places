<?php

use yii\db\Migration;

/**
 * Class m220402_113204_fill_rooms
 */
class m220402_113204_fill_rooms extends Migration
{
    private $data = [
        ['type' => 'Одноместный', 'count' => 2],
        ['type' => 'Двуместный', 'count' => 4],
        ['type' => 'Люкс', 'count' => 3],
        ['type' => 'Де-Люк', 'count' => 5],
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach ($this->data as $room) {
            $this->insert('rooms', $room);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        foreach ($this->data as $room) {
            $this->delete('rooms', $room);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220402_113204_fill_rooms cannot be reverted.\n";

        return false;
    }
    */
}
