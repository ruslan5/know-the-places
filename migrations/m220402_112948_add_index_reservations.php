<?php

use yii\db\Migration;

/**
 * Class m220402_112948_add_index_reservations
 */
class m220402_112948_add_index_reservations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Yii::$app->db->createCommand('
            CREATE INDEX searching_date_intersects ON reservations USING gist(daterange(date_from, date_to));
        ')->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('searching_date_intersects', 'reservations');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220402_112948_add_index_reservations cannot be reverted.\n";

        return false;
    }
    */
}
