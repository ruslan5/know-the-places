<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Reservation;
use app\models\Room;
use DateTimeImmutable;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GenerateController extends Controller
{
    /**
     * This command fills database by reservations
     *
     * @param string $count the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($count = 1000)
    {
        $scriptTimeStart = microtime(true);
        $rooms = Room::find()->all();

        for ($i = 0; $i < $count; $i++) {
            $makingTimeStart = microtime(true);
            $roomIndex = rand(0, count($rooms) - 1);
            $reservation = new Reservation();
            $dateFrom = (new DateTimeImmutable())->modify('+' . rand(0, 720) . ' days');

            $reservation->date_from = $dateFrom->format('Y-m-d');
            $reservation->date_to = $dateFrom->modify('+' . rand(0, 14) . ' days')->format('Y-m-d');
            $reservation->room_id = $rooms[$roomIndex]->id;
            $reservation->name = 'Руслан';
            $reservation->email = 'ruslan@yandex.ru';

            if ($reservation->save()) {
                echo 'Reservation has been created for ' . (microtime(true) - $makingTimeStart) . ' seconds' . PHP_EOL;
            } else {
                echo 'Error: there are not ' . $rooms[$roomIndex]->type . ' rooms' . PHP_EOL;
            }
        }

        echo PHP_EOL . 'Executed for ' . (microtime(true) - $scriptTimeStart) . ' seconds';

        return ExitCode::OK;
    }
}
