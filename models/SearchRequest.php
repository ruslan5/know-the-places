<?php

namespace app\models;

use DateTimeImmutable;
use yii\base\Model;

class SearchRequest extends Model
{
    /** @var string|null $from */
    public $from;

    /** @var string|null $to */
    public $to;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // two dates
            [['from', 'to'], 'required'],
            [['from', 'to'], 'date', 'format' => 'php:Y-m-d'],
            // from is not bigger to
            [['from'], 'validateFrom'],
            // to cant be more then from + 1 year
            [['to'], 'validateTo'],
        ];
    }

    /**
     * From must be less to or equals
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateFrom($attribute, $params)
    {
        if (!$this->hasErrors()) {

            if ($this->from > $this->to) {
                $this->addError('from', 'From is bigger to');
            }
        }
    }

    /**
     * To cant be more then from + 1 year
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateTo($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $dateFrom = new DateTimeImmutable($this->from);
            $dateTo = new DateTimeImmutable($this->to);
            $differenceInSec = $dateTo->getTimestamp() - $dateFrom->getTimestamp();

            if (intval(ceil($differenceInSec / 86400)) > 365) {
                $this->addError('to', 'To cant be more then from + 1 year');
            }
        }
    }

    /**
     * Gets range of dates from-to
     *
     * @return array
     * @throws \Exception
     */
    public function getDateRange(): array
    {
        $result = [];

        if (!is_null($this->from)) {
            $result[] = new DateTimeImmutable($this->from);
            $result[] = new DateTimeImmutable($this->to);
        }

        return $result;
    }
}
