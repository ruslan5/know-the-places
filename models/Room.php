<?php

namespace app\models;

use DateTimeImmutable;
use DateTimeZone;
use Yii;
use yii\base\Exception;
use yii\db\Query;

/**
 * This is the model class for table "rooms".
 *
 * @property int $id
 * @property string $type
 * @property int $count
 *
 * @property Reservation[] $reservations
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'count'], 'required'],
            [['count'], 'default', 'value' => null],
            [['count'], 'integer'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'count' => 'Count',
        ];
    }

    /**
     * Gets query for [[Reservations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['room_id' => 'id']);
    }

    /**
     * Checks free rooms
     */
    public function hasFree(DateTimeImmutable $dateFrom, DateTimeImmutable $dateTo, bool $dbForce = false): bool
    {
        return $this->getFreeCount($dateFrom, $dateTo, $dbForce) > 0;
    }

    /**
     * Gets count of free rooms
     */
    public function getFreeCount(DateTimeImmutable $dateFrom, DateTimeImmutable $dateTo, bool $dbForce = false): int
    {
        return $this->count - $this->getReservedCount($dateFrom, $dateTo, $this->count, $dbForce);
    }

    /**
     * Gets reserved rooms in a period
     *
     * @param DateTimeImmutable $dateFrom
     * @param DateTimeImmutable $dateTo
     * @return int
     * @throws Exception
     */
    public function getReservedCount(DateTimeImmutable $dateFrom, DateTimeImmutable $dateTo, ?int $maxPossible = null, bool $dbForce = false): int
    {
        $max = 0;

        if (is_null($this->id)) {
            throw new Exception('Cant detect reservation cause room does not exist');
        }

        do {
            $reservedCount = Reservation::getCount($dateFrom, $this->id, $dbForce);
            $max = $max < $reservedCount ? $reservedCount : $max;

            if (!is_null($maxPossible) && $max >= $maxPossible) {
                break;
            }

            $dateFrom = $dateFrom->modify('+1 day');
        } while ($dateFrom->format('Y-m-d') <= $dateTo->format('Y-m-d'));

        return $max;
    }
}
