<?php

namespace app\models;

use DateTimeImmutable;
use DateTimeZone;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "reservations".
 *
 * @property int $id
 * @property string $date_from
 * @property string $date_to
 * @property int $room_id
 * @property string $name
 * @property string $email
 *
 * @property Room $room
 */
class Reservation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reservations';
    }

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'cache']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_from', 'date_to', 'room_id', 'name', 'email'], 'required'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
            [['room_id'], 'default', 'value' => null],
            [['room_id'], 'integer'],
            [['name', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['date_from'], 'validateDateFrom'],
            [['date_to'], 'validateDateTo'],
            [['room_id'], 'checkRoomFree']
        ];
    }

    /**
     * Caches count of rooms reserved at the day
     *
     * @throws \Exception
     */
    public function cache()
    {
        // it is better to do this in a job
        $now = (new DateTimeImmutable())->setTime(0, 0);
        $dateFrom = new DateTimeImmutable($this->date_from);
        $dateTo = new DateTimeImmutable($this->date_to);

        do {
            self::getCount($dateFrom, $this->room_id, true);

            $dateFrom = $dateFrom->modify('+1 day');
        } while ($dateFrom->format('Y-m-d') <= $dateTo->format('Y-m-d'));
    }

    /**
     * Gets count of reservation at one day
     *
     * @param DateTimeImmutable $day
     * @param int $roomId
     * @param bool|bool $dbForce
     * @return int
     */
    public static function getCount(DateTimeImmutable $day, int $roomId, bool $dbForce = true): int
    {
        $key = self::getCacheKey($day);
        $cached = Yii::$app->cache->get($key);
        $toCache = [];

        // get from cache if it is there
        if ($cached) {
            $cached = json_decode($cached, true);

            if (is_array($cached)) {
                $toCache = $cached;

                if (!$dbForce && key_exists($roomId, $cached) && is_int($cached[$roomId])) {
                    return $cached[$roomId];
                }
            }
        }

        // get from db
        $timezone = new DateTimeZone('UTC');
        $now = (new DateTimeImmutable())->setTimezone($timezone)->setTime(0, 0);
        $day = $day->setTimezone($timezone);
        $dayStr = $day->format('Y-m-d');
        $query = new Query();

        $query
            ->select('COUNT(rs.id) as count')
            ->from('reservations rs')
            ->where("rs.room_id = $roomId AND daterange(rs.date_from, rs.date_to, '[]') && daterange('$dayStr', '$dayStr', '[]')")
            ->limit(1);

        $count = $query->one()['count'];

        // remember value from db
        $toCache[$roomId] = $count;
        $duration = $day->getTimestamp() - $now->getTimestamp();
        $duration = $duration > 86400 ? $duration : 86400;

        Yii::$app->cache->set($key, json_encode($toCache), $duration);

        return $count;
    }

    /**
     * Gets key-name for storing system info
     *
     * @param DateTimeImmutable $date
     * @return string
     */
    public static function getCacheKey(DateTimeImmutable $date)
    {
        return sprintf('reservation-%s', $date->format('Y-m-d'));
    }

    /**
     * DateFrom must be bigger or equals now
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateDateFrom($attribute, $params)
    {
        if (!$this->hasErrors()) {

            if ($this->isNewRecord) {
                $now = (new DateTimeImmutable())->format('Y-m-d');

                if ($now > $this->date_from) {
                    $this->addError('date_from', 'Date From must be bigger or equal to now');
                }
            }
        }
    }

    /**
     * DateTo must be bigger or equal DateFrom
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateDateTo($attribute, $params)
    {
        if (!$this->hasErrors()) {

            if ($this->date_from > $this->date_to) {
                $this->addError('date_to', 'Date To is less Date From');
            }
        }
    }

    /**
     * Checks room
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     * @throws \yii\base\Exception
     */
    public function checkRoomFree($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $room = Room::findOne($this->room_id);

            if (!is_null($room)) {
                $dateFrom = new DateTimeImmutable($this->date_from);
                $dateTo = new DateTimeImmutable($this->date_to);

                if (!$room->hasFree($dateFrom, $dateTo)) {
                    $this->addError('room_id', 'All rooms of the type have been reserved');
                }
            } else {
                $this->addError('room_id', 'The room type does not exist');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'room_id' => 'Room Type',
            'name' => 'Name',
            'email' => 'Email',
        ];
    }

    /**
     * Gets query for [[Room]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }
}
