<?php

namespace app\controllers;

use app\models\Reservation;
use app\models\Room;
use app\models\SearchRequest;
use yii\helpers\Url;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays search page.
     *
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $requestModel = new SearchRequest();
        $rooms = [];

        if ($requestModel->load(Yii::$app->request->get(), '') && $requestModel->validate()) {
            $dateRange = $requestModel->getDateRange();

            if (count($dateRange)) {
                $rooms = array_reduce(
                    Room::find()->all(),
                    function ($rooms, $room) use($dateRange) {
                        $freeRoomsCount = $room->getFreeCount(...$dateRange);

                        if ($freeRoomsCount > 0) {
                            $rooms[] = [
                                'type' => $room->type,
                                'free_count' => $freeRoomsCount
                            ];
                        }

                        return $rooms;
                    },
                    []
                );
            }
        }

        $dataProvider = new ArrayDataProvider(['allModels' => $rooms]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'requestModel' => $requestModel,
        ]);
    }

    /**
     * Displays reserve page
     *
     * @return string|Response
     */
    public function actionReserve()
    {
        $requestModel = new Reservation();

        if ($requestModel->load(Yii::$app->request->post(), '') && $requestModel->save()) {
            return $this->goBack(Url::to(['site/reserve']));
        }

        $rooms = Room::find()->all();
        $reservationQuery = Reservation::find()->orderBy(['id' => SORT_DESC])->with('room');
        $dataProvider = new ActiveDataProvider(['query' => $reservationQuery]);

        return $this->render('reserve', [
            'requestModel' => $requestModel,
            'rooms' => $rooms,
            'dataProvider' => $dataProvider,
        ]);
    }
}
