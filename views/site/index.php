<?php

/** @var yii\web\View $this */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var app\models\SearchRequest $requestModel */

use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\bootstrap4\ActiveForm;use yii\helpers\Url;

$this->title = 'Доступные номера';
?>

<h1>Поиск</h1>

<?php

$form = ActiveForm::begin([
    'id' => 'search-form',
    'layout' => 'horizontal',
    'method' => 'GET',
    'action' => Url::to(['site/index']),
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
        'labelOptions' => ['class' => 'col-lg-1 col-form-label mr-lg-3'],
        'inputOptions' => ['class' => 'col-lg-3 form-control'],
        'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
    ],
]);

?>

<?= $form->field($requestModel, 'from')->input('date', ['name' => 'from']) ?>

<?= $form->field($requestModel, 'to')->input('date', ['name' => 'to']) ?>

<?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>

<h1 class="mt-4"><?= $this->title ?></h1>

<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => ['type', 'free_count']
]);

?>
