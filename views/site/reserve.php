<?php

/** @var yii\web\View $this */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var app\models\Reservation $requestModel */
/** @var app\models\Room[] $rooms */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Бронирование';
?>

<h1><?= $this->title ?></h1>

<?php

$form = ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'method' => 'POST',
    'action' => Url::to(['site/reserve']),
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
        'labelOptions' => ['class' => 'col-lg-2 col-form-label mr-lg-3'],
        'inputOptions' => ['class' => 'col-lg-3 form-control'],
        'errorOptions' => ['class' => 'col-lg-6 invalid-feedback'],
    ],
]);

?>

<?= $form->field($requestModel, 'date_from')->input('date', ['name' => 'date_from']) ?>

<?= $form->field($requestModel, 'date_to')->input('date', ['name' => 'date_to']) ?>

<?= $form->field($requestModel, 'room_id')->dropdownList(ArrayHelper::map($rooms, 'id', 'type'), ['name' => 'room_id']) ?>

<?= $form->field($requestModel, 'name')->input('text', ['name' => 'name']) ?>

<?= $form->field($requestModel, 'email')->input('text', ['name' => 'email']) ?>

<?= Html::submitButton('Забронировать', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>

<h1 class="mt-4">Недавние</h1>

<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => ['id', 'date_from', 'date_to', 'room.type', 'name', 'email']
]);

?>
