FROM yiisoftware/yii2-php:7.4-apache

# memcached
RUN set -ex \
    && apt-get update \
    && apt-get install -y libmemcached-dev zlib1g-dev \
    && rm -rf /var/lib/apt/lists/* \
    && MEMCACHED="`mktemp -d`" \
    && curl -skL https://github.com/php-memcached-dev/php-memcached/archive/master.tar.gz | tar zxf - --strip-components 1 -C $MEMCACHED \
    && docker-php-ext-configure $MEMCACHED \
    && docker-php-ext-install $MEMCACHED \
    && rm -rf $MEMCACHED

COPY --chown=www-data . /app/

WORKDIR /app

RUN composer install
